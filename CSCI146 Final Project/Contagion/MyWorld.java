import greenfoot.*;  // (World, Actor, GreenfootImage, Greenfoot and MouseInfo)
import java.util.List;
/**
 * The world in which the people will move
 * 
 * @author jaronm@email.uscb.edu
 * @version 1.0
 */
public class MyWorld extends World
{
    private static final int NUMBER_OF_PEOPLE = 150;
    private static final int[] PERCENT_ISOLATE = { 0, 25, 50, 75, 90};
    private static final Color[] COLORS = { Color.RED, Color.BLUE, 
                                            Color.MAGENTA,Color. YELLOW, 
                                            Color.BLACK};
    // makes it so that the program
    // will change the color of the 
    // graph each time it runs
    private static final int SCALE_Y = 3;

    private int maxInfected = 0;
    private int xOffset = 0;
    private int run = 0; // part of Episode 15

    private static final int WORLD_WIDTH = 1000;
    private static final int WORLD_HEIGHT = 600; // A suggested solution to having to re-type
    // the width and height of the world and figure out
    // half of them if I continously change the numbers

    /**
     * Create the world and populate it with people
     * 
     */
    public MyWorld()
    {    
        // Create a new world with 1000x600 cells with a cell size of 1x1 pixels.
        super(1000, 600, 1); 
        reset();
    } // end constructor

    private void reset() // Episode 15: comparing graphs
    // the program will effectively reset when the graph
    // reaches the edge of the screen / all actors
    // of the Person class are healed
    {
        xOffset = 0;
        getBackground().setColor(COLORS[run]);
        populate(PERCENT_ISOLATE[run]);

        getBackground().drawString("% isolating: " + PERCENT_ISOLATE[run] + "%",
            700, 20 + 25*run);
        // Episode 16: The isolating % at the top of the screen 
        // will now display multiple times and be appropriately
        // color coded to match the graph 
    }  // end method reset
    public void act() // Episode 15
    {
        displayInfo();   
        if (xOffset > getWidth()) {
            nextRun();
        } // end if statement
    } // end method act

    private void nextRun() // Episode 16
    {
        run++;
        if ( run == PERCENT_ISOLATE.length) {
            removeObjects(getObjects(Person.class));
            Greenfoot.stop();
        }
        else {
            reset();
        } // end if-else statement 
    } // end method nextRun
    /**
     * Display the statistics of this population. // Episode 9
     * ( To be precise, this will display a counter at the top of the screen of how many people are ifnected, that will 
     * eventually cut down as the actors of the Person class in the world begin to heal themselves
     */
    private void displayInfo()
    {
        if ( Person.getNumberInfected() > maxInfected) {
            maxInfected = Person.getNumberInfected();
        } // end if statement

        showText("Infected: " + Person.getNumberInfected(),WORLD_WIDTH/6, WORLD_HEIGHT/50);
        showText("Max: " + maxInfected, WORLD_WIDTH/3, WORLD_HEIGHT/50);

        plotValue(Person.getNumberInfected());
    }  // end method displayInfo

    //episode 11
    /** 
     * Plot a single value by drawing on the world background
     */
    private void plotValue(int value)
    {
        int yOffset = getHeight() - value * SCALE_Y;

        getBackground().drawLine(xOffset, yOffset, xOffset, yOffset - 4);

        xOffset++;
    } // end method plotValue

    /**
     * Populate the world with with all of the initial actors 
     */
    public void populate(int percentIsolating)
    {
        removeObjects (getObjects(Person.class));
        Person.reset();

        Person patientZero = new Person(false); // patientZero is just known as person in the videos
        patientZero.infect();
        addObject(patientZero, WORLD_WIDTH/2, WORLD_HEIGHT/2);

        for(int i = 0; i< NUMBER_OF_PEOPLE; i++) {
            int x = Greenfoot.getRandomNumber(getWidth());
            int y = Greenfoot.getRandomNumber(getHeight());

            boolean isolate = Greenfoot.getRandomNumber(100) < percentIsolating;

            addObject(new Person(isolate), x, y); 
        } // end for loop
    } // end method populate
} // end public class MyWorld
