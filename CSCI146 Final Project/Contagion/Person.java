import greenfoot.*;  // (World, Actor, GreenfootImage, Greenfoot and MouseInfo)

/**
 * Write a description of class Person here.
 * 
 * @author jaronm@email.uscb.edu
 * @version 1.0 / 29 April 2020
 */
public class Person extends Actor
{
    private static final int INFECTION_TIME= 200;

    private int infection = 0;
    private InfectionStatus status = InfectionStatus.SUSCEPTIBLE;
    //private boolean isImmune = false;
    private boolean isIsolating; // episode 12
    private static int numInfected = 0;

    /**
     * Return the number of infected persons.
     */
    public static int getNumberInfected()
    {
        return numInfected;
    } // end method getNumberInfected

    /**
     * sets number of people to 0 ( since  numInfected was set to static,
     * it was not automatically being reset every time the program was re-ran
     * without re-initializing 
     */
    public static void reset()
    {
        numInfected = 0;
    } // end method reset
    /**
     *  Create a person with random direction of movement.
     */
    public Person(boolean isolating)
    {
        turn(Greenfoot.getRandomNumber(360));
        isIsolating = isolating;
        
    } // end constructor
   
    public void addedToWorld(World world) // Episode 20: Inheritance
    {
        if (Greenfoot.getRandomNumber(100) < 10) {
            Child child = new Child(isIsolating, this);
            getWorld().addObject(child, getX()+16, getY());
        } // end if statement (Episode 20)
    } // end method addedToWorld
    
    /**
     * Act - do whatever the Person wants to do. This method is called whenever
     * the 'Act' or 'Run' button gets pressed in the environment.
     */
    public void act() 
    {
        if(!isIsolating) {
            move();
        } // end if statement
        if(isInfected()) {
            infectOthers();
            heal();
        } // end if statement
        // makes it so that other actors can only be infected by an actor
        // that are infected
    }    // end method act

    /**
     * Move the person randomly on screen
     */
    private void move()
    {
        move(3);
        if (isAtEdge()) {
            turn(87);
        } // end if statement
        if (Greenfoot.getRandomNumber(100) < 20) {
            turn(Greenfoot.getRandomNumber(90)-30);
        } // end if statement
    } // end method move

    /**
     * Check whether person is intersecting another person. If they are, infect them.
     */
    private void infectOthers()
    {
        Person other = (Person) getOneIntersectingObject(Person.class);
        if (other != null) {
            other.infect();
        } // end if statement
    } // end method infectOthers

    /**
     * Try to infect this person. If we are not immune or infected already, we will be infected now // Episode 10
     */
    public void infect()
    {
        if (status == InfectionStatus.SUSCEPTIBLE) {
            infection = INFECTION_TIME;
            numInfected++;
            //setImage("ppl3.png");
            //isImmune = true;
            setStatus(InfectionStatus.INFECTED);
        } // end if statement
    } // end method infect

    public boolean isInfected()
    {
        //return infection > 0;         // makes it so that other actors can only be infected by an actor
        // that are infected
        return status == InfectionStatus.INFECTED;
    } // end method isInfected

    /**
     * If we are infected, execute the healing process. If infection reaches zero we are immune.
     */
    private void heal()
    {
        if (isInfected()) {
            infection--;
            if(infection == 0) {
                //status = InfectionStatus.IMMUNE;
                // setImage("ppl2.png");
                setStatus(InfectionStatus.IMMUNE);
                numInfected--;
                //setImage(status.getImage());
            } // end if statement
        } // end if statement
    } // end method heal

    private void setStatus(InfectionStatus newStatus)
    {
        status = newStatus;
        changeImage(status.getImage());
    } // end method setStatus
    
    protected void changeImage(String imageName)
    {
        setImage(imageName);
    } // end method changeImage
} // end public class Person
