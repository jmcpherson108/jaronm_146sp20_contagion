/**
 * Enumeration of possible infection states
 * Episode 18 ( the beginning of user suggestions) : Introduction to enums
 * 
 * @author jaronm@email.uscb.edu 
 * @version 1.0 6 May 2020
 */
public enum InfectionStatus  
{ 
    SUSCEPTIBLE("ppl1.png"), INFECTED("ppl3.png"), IMMUNE("ppl2.png");
    
    private String image;
    
    private InfectionStatus(String imageName)
    {
        image = imageName;
    } // end constructor InfectionStatus
    
    public String getImage()
    {
        return image;
    } // end method getImage
}  // end enum InfectionStatus
