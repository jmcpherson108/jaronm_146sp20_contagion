import greenfoot.*;  // (World, Actor, GreenfootImage, Greenfoot and MouseInfo)

/**
 * Write a description of class Child here.
 * Episode 20: Inheritance
 * 
 * @jaronm@email.uscb.edu
 * @version 1.0 / 6 May 2020
 */
public class Child extends Person
{
    private Person parent;

    public Child(boolean isolating, Person parent)
    {
        super(isolating);
        this.parent = parent;
    } // end constructor child

    /**
     * Move the person randomly on screen
     */
    private void move()
    {
        turnTowards(parent.getX()+16, parent.getY());
        move(3);
    } // end method move

    protected void changeImage(String imageName)
    {
        setImage("child" + imageName);
    } // end method changeImage
    // /**
    // * Act - do whatever the Child wants to do. This method is called whenever
    // * the 'Act' or 'Run' button gets pressed in the environment.
    // */
    // public void act() 
    // {
    // // Add your action code here.
    // }    // end method act
    // The Act method was removed so that the child class would just inherit
    // the parent's act method
} // end class child
